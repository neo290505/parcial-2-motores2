using UnityEngine;

public class CameraMover : MonoBehaviour
{
    public GameObject camera; // La c�mara principal
    public Transform targetPosition; // La nueva posici�n de la c�mara
    public float moveSpeed = 2.0f; // La velocidad de movimiento de la c�mara

    private bool shouldMove = false;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            shouldMove = true;
        }
    }

    void Update()
    {
        if (shouldMove && camera != null && targetPosition != null)
        {
            camera.transform.position = Vector3.Lerp(camera.transform.position, new Vector3(targetPosition.position.x, targetPosition.position.y, camera.transform.position.z), moveSpeed * Time.deltaTime);

            // Detener el movimiento cuando la c�mara est� cerca de la posici�n objetivo
            if (Vector3.Distance(camera.transform.position, new Vector3(targetPosition.position.x, targetPosition.position.y, camera.transform.position.z)) < 0.1f)
            {
                shouldMove = false;
            }
        }
    }
}
