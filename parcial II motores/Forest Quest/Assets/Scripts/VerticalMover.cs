using UnityEngine;

public class VerticalMover : MonoBehaviour
{
    public float moveDistance = 1.0f; // La distancia que se mover� hacia arriba y hacia abajo
    public float moveSpeed = 1.0f; // La velocidad del movimiento

    private Vector3 initialPosition;
    private bool movingUp = true;

    void Start()
    {
        initialPosition = transform.position;
    }

    void Update()
    {
        float newY = transform.position.y + (movingUp ? 1 : -1) * moveSpeed * Time.deltaTime;

        if (movingUp && newY >= initialPosition.y + moveDistance)
        {
            newY = initialPosition.y + moveDistance;
            movingUp = false;
        }
        else if (!movingUp && newY <= initialPosition.y - moveDistance)
        {
            newY = initialPosition.y - moveDistance;
            movingUp = true;
        }

        transform.position = new Vector3(transform.position.x, newY, transform.position.z);
    }
}
