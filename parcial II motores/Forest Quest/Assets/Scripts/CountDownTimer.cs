using UnityEngine;
using TMPro;

public class CountDownTimer : MonoBehaviour
{
    public float startTime = 60.0f; // Tiempo inicial de la cuenta regresiva en segundos
    public TMP_Text countdownText; // El TextMeshProUGUI que muestra el tiempo restante
    public Canvas gameOverCanvas; // El Canvas que se mostrar� cuando el tiempo llegue a 0

    private float currentTime;
    private bool isTimerRunning = true;

    void Start()
    {
        currentTime = startTime;
        gameOverCanvas.gameObject.SetActive(false); // Asegurarse de que el Canvas de Game Over est� desactivado al inicio
    }

    void Update()
    {
        if (isTimerRunning)
        {
            currentTime -= Time.deltaTime;
            if (currentTime <= 0)
            {
                currentTime = 0;
                GameOver();
            }

            UpdateCountdownText();
        }
    }

    void UpdateCountdownText()
    {
        int minutes = Mathf.FloorToInt(currentTime / 60);
        int seconds = Mathf.FloorToInt(currentTime % 60);
        countdownText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    void GameOver()
    {
        gameOverCanvas.gameObject.SetActive(true); // Mostrar el Canvas de Game Over
        Time.timeScale = 0; // Congelar el juego
        isTimerRunning = false; // Detener la cuenta regresiva
        this.enabled = false; // Desactivar este script para detener la cuenta regresiva
    }

    public void StopTimer()
    {
        isTimerRunning = false;
    }
}
