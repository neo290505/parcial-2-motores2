using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    void Update()
    {
        // Reiniciar la escena actual al presionar "R"
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        // Cargar la escena del men� al presionar "Esc"
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Menu");
            SceneManager.UnloadScene("Juego");
        }

        if (Input.GetKeyDown(KeyCode.N))
        {
            SceneManager.LoadScene("Level 2");
            SceneManager.UnloadScene("Juego");
        }
    }
}
