using UnityEngine;

public class TriggerPlatformActivatorNew : MonoBehaviour
{
    public GameObject[] platforms; // Las plataformas que ser�n activadas
    public SpriteRenderer indicatorSprite; // El sprite que indica el �rea del trigger
    public Color activatedColor = Color.green; // Color cuando est� activado
    public Color deactivatedColor = Color.red; // Color cuando no est� activado
    public GameObject objectToChangeColor; // El nuevo GameObject cuyo color cambiar�

    private bool isActivated = false;

    void Start()
    {
        // Establecer el color inicial del indicador
        if (indicatorSprite != null)
        {
            indicatorSprite.color = deactivatedColor;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isActivated && collision.gameObject.CompareTag("MovableObject"))
        {
            ActivatePlatforms();
            if (indicatorSprite != null)
            {
                indicatorSprite.color = activatedColor;
            }
            if (objectToChangeColor != null)
            {
                ChangeObjectColor(Color.green);
            }
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (isActivated && collision.gameObject.CompareTag("MovableObject"))
        {
            DeactivatePlatforms();
            if (indicatorSprite != null)
            {
                indicatorSprite.color = deactivatedColor;
            }
        }
    }

    void ActivatePlatforms()
    {
        foreach (GameObject platform in platforms)
        {
            platform.SetActive(true);
        }
        isActivated = true;
    }

    void DeactivatePlatforms()
    {
        foreach (GameObject platform in platforms)
        {
            platform.SetActive(false);
        }
        isActivated = false;
    }

    void ChangeObjectColor(Color newColor)
    {
        SpriteRenderer spriteRenderer = objectToChangeColor.GetComponent<SpriteRenderer>();
        if (spriteRenderer != null)
        {
            spriteRenderer.color = newColor;
        }
        else
        {
            Renderer renderer = objectToChangeColor.GetComponent<Renderer>();
            if (renderer != null)
            {
                renderer.material.color = newColor;
            }
        }
    }
}
