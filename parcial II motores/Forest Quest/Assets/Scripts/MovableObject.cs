using UnityEngine;

public class MovableObject : MonoBehaviour
{
    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Vector2 pushDirection = collision.transform.position - transform.position;
            rb.velocity = new Vector2(pushDirection.x * 2, rb.velocity.y);
        }
    }
}
