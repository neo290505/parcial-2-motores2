using UnityEngine;

public class HorizontalMover : MonoBehaviour
{
    public float moveDistance = 1.0f; // La distancia que se mover� hacia la izquierda y derecha
    public float moveSpeed = 1.0f; // La velocidad del movimiento

    private Vector3 initialPosition;
    private bool movingRight = true;

    void Start()
    {
        initialPosition = transform.position;
    }

    void Update()
    {
        float newX = transform.position.x + (movingRight ? 1 : -1) * moveSpeed * Time.deltaTime;

        if (movingRight && newX >= initialPosition.x + moveDistance)
        {
            newX = initialPosition.x + moveDistance;
            movingRight = false;
        }
        else if (!movingRight && newX <= initialPosition.x - moveDistance)
        {
            newX = initialPosition.x - moveDistance;
            movingRight = true;
        }

        transform.position = new Vector3(newX, transform.position.y, transform.position.z);
    }
}
