using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VolverAMenú : MonoBehaviour
{
    public void VolverAMenu ()
    {

        SceneManager.LoadScene("Menu");
        SceneManager.UnloadScene("ComoJugar");


    }
}
