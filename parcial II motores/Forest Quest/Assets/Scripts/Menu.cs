using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
   public void Jugar ()
    {

        SceneManager.LoadScene("Juego");
        SceneManager.UnloadScene("Menu");

    }

    public void ComoJugar ()
    {

        SceneManager.LoadScene("ComoJugar");
        SceneManager.UnloadScene("Menu");

    }


    public void Salir()
    {

        Application.Quit();
        Debug.Log("Salir");
    }
}
