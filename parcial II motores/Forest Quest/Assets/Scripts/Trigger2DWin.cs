using UnityEngine;

public class Trigger2DWin : MonoBehaviour
{
    public Canvas winCanvas; // El Canvas que se mostrar� al ganar
    public CountDownTimer countdownTimer; // Referencia al script CountdownTimer

    void Start()
    {
        // Asegurarse de que el Canvas de ganar est� desactivado al inicio
        if (winCanvas != null)
        {
            winCanvas.gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        // Comprobar si el objeto que entr� en el trigger tiene el tag "Player"
        if (collision.gameObject.CompareTag("Player"))
        {
            ShowWinCanvas();
            if (countdownTimer != null)
            {
                countdownTimer.StopTimer();
            }
        }
    }

    void ShowWinCanvas()
    {
        if (winCanvas != null)
        {
            winCanvas.gameObject.SetActive(true); // Mostrar el Canvas de ganar
            Time.timeScale = 0; // Congelar el juego
        }
    }
}
