using UnityEngine;
using UnityEngine.SceneManagement;

public class Bullet : MonoBehaviour
{
    public float speed = 5f; // Velocidad de la bala
    public float maxDistance = 10f; // Distancia m�xima antes de que la bala desaparezca

    private Vector3 startPosition;
    private ObjectPool objectPool;

    void Start()
    {
        startPosition = transform.position;
        objectPool = FindObjectOfType<ObjectPool>();
    }

    void Update()
    {
        // Mover la bala
        transform.Translate(Vector2.right * speed * Time.deltaTime);

        // Verificar si la bala ha recorrido la distancia m�xima
        if (Vector2.Distance(startPosition, transform.position) > maxDistance)
        {
            objectPool.ReturnObject(gameObject); // Devolver la bala al pool
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Door")) { 
            // Devolver la bala al pool al colisionar con algo
            objectPool.ReturnObject(gameObject);
            
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        }
    }
}
