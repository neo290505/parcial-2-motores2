using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float jumpForce = 10f;
    public LayerMask groundLayer;
    public GameObject bulletPrefab; // Prefab de la bala
    public Transform firePoint; // Punto desde donde se disparar� la bala
    public float bulletSpeed = 20f; // Velocidad de la bala

    private Rigidbody2D rb;
    private bool isGrounded;
    private bool hasKey;
    private float moveInput; // Almacena la entrada de movimiento
    private Vector2 lastMoveDirection = Vector2.right; // Direcci�n de movimiento por defecto

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        // Movimiento horizontal
        moveInput = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(moveInput * moveSpeed, rb.velocity.y);

        // Actualizar la direcci�n de movimiento si el jugador se est� moviendo
        if (moveInput != 0)
        {
            lastMoveDirection = moveInput > 0 ? Vector2.right : Vector2.left;
        }

        // Saltar
        if (isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        }

        // Disparar
        if (Input.GetKeyDown(KeyCode.E))
        {
            Shoot();
        }
    }

    void Shoot()
    {
        // Usar la �ltima direcci�n de movimiento para disparar
        Vector2 direction = lastMoveDirection;

        // Instanciar la bala en la posici�n y rotaci�n del punto de disparo
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, Quaternion.identity);

        // Obtener el componente Rigidbody2D de la bala para aplicarle fuerza
        Rigidbody2D rbBullet = bullet.GetComponent<Rigidbody2D>();
        rbBullet.velocity = direction * bulletSpeed;

        // Rotar la bala para que apunte hacia la direcci�n del disparo
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        bullet.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            isGrounded = false;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Key"))
        {
            hasKey = true;
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("Door") && hasKey)
        {
            Destroy(collision.gameObject);
        }
    }
}
