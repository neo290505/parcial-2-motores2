using UnityEngine;
using UnityEngine.SceneManagement; // Necesario para reiniciar la escena

public class RestartAtTouch : MonoBehaviour
{
    // M�todo que se llama cuando otro collider entra en el trigger
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Verificar si el objeto que colision� tiene el tag del jugador
        if (collision.CompareTag("Player"))
        {
            // Reiniciar el nivel actual
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
