using UnityEngine;

public class PlayerBullet : MonoBehaviour
{
    public string targetTag = "Target";
    public string wallTag = "derap";// Tag del objeto con el que se destruir� el proyectil

    void OnCollisionEnter2D(Collision2D collision)
    {
        // Comprobar si el objeto con el que colision� tiene el tag especificado
        if (collision.gameObject.CompareTag(targetTag))
        {
            // Destruir el proyectil
            Destroy(gameObject);
        }

        if (collision.gameObject.CompareTag("derap"))
        {
            // Destruir el proyectil
            Destroy(gameObject);
        }
    }
}
