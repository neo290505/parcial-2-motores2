using UnityEngine;

public class TriggerPlatformActivatorNew1 : MonoBehaviour
{
    public GameObject[] platforms; // Las plataformas que ser�n desactivadas
    public SpriteRenderer indicatorSprite; // El sprite que indica el �rea del trigger
    public Color activatedColor = Color.green; // Color cuando est� activado
    public Color deactivatedColor = Color.red; // Color cuando no est� activado

    private bool isActivated = false;

    void Start()
    {
        // Establecer el color inicial del indicador
        if (indicatorSprite != null)
        {
            indicatorSprite.color = deactivatedColor;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isActivated && collision.gameObject.CompareTag("MovableObject"))
        {
            DeactivatePlatforms();
            if (indicatorSprite != null)
            {
                indicatorSprite.color = deactivatedColor; // Color rojo al desactivar
            }
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (isActivated && collision.gameObject.CompareTag("MovableObject"))
        {
            // Aqu� podr�as implementar alguna l�gica adicional si el objeto movible sale del trigger
        }
    }

    void DeactivatePlatforms()
    {
        foreach (GameObject platform in platforms)
        {
            platform.SetActive(false); // Desactivar cada plataforma
        }
        isActivated = true; // Marcar como desactivado
    }
}